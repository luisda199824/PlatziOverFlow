import express from 'express';
import {
	required,
	questionMiddleware,
	questionsMiddleware
} from '../middleware';

const app = express.Router();

// GET /api/questions
app.get('/', questionsMiddleware, (req, res) => {
	res.status(200).json(req.questions);
});

// GET /api/quetions/:id
app.get('/:id', questionMiddleware, (req, res) => res.status(200).json(req.question));

// POST /api/questions
app.post('/', required, questionsMiddleware, (req, res) => {
	const q = req.body;
	q._id = +new Date();
	q.user = req.user;
	q.createdAt = new Date();
	q.answers = [];
	req.questions.push(q);

	res.status(201).json(q);
});

// POST /:id/answers
app.post('/:id/answers', required, questionMiddleware, (req, res) => {
	const answer = req.body;
	const q = req.question;
	answer.createdAt = new Date();
	answer.user = req.user;
	q.answers.push(answer);

	res.status(201).json(answer);
});

export default app;
