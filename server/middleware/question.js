const currentUser = {
	firstName: 'Luis',
	lastName: 'Ocampo',
	email: 'luis@gmail.com',
	password: '123456'
};

const question = {
	_id: 1,
	title: '¿Cómo reutilizo un componente en Android?',
	description: 'Miren esta es mi pregunta',
	createdAt: new Date(),
	icon: 'devicon-android-plain',
	answers: [],
	user: currentUser
};

const questions = new Array(10).fill(question);

export const questionsMiddleware = (req, res, next) => {
	req.questions = questions;
	next();
};

export const questionMiddleware = (req, res, next) => {
  const { id } = req.params;
	req.question = questions.find(question => question._id === +id);

	next();
};
