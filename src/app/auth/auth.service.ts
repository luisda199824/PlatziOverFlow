import { Injectable } from '@angular/core';
import urljoin from 'url-join';
import { environment } from '../../environments/environment';
import { User } from './user.model';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthService {
  userUrl: string;
  currentUser?: User;

  constructor(
    private http: Http,
    private router: Router,
    public snackbar: MatSnackBar
  ) {
    this.userUrl = urljoin(environment.apiUrl, 'auth');

    if (this.isLoggedIn()) {
      const { userId, firstName, lastName, email } = JSON.parse(localStorage.getItem('user'));
      this.currentUser = new User(
        email,
        null,
        firstName,
        lastName,
        userId
      );
    }
  }

  signup(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({ 'Content-Type' : 'application/json' });

    return this.http.post(urljoin(this.userUrl, 'signup'), body, { headers })
      .map((res: Response) => {
        const json = res.json();
        this.login(json);
        return json;
      })
      .catch((err: Response) => {
        console.log(err);
        return Observable.throw(err.json());
      });
  }

  signin(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({ 'Content-Type' : 'application/json' });

    return this.http.post(urljoin(this.userUrl, 'signin'), body, { headers })
      .map((res: Response) => {
        const json = res.json();
        this.login(json);
        return json;
      })
      .catch((err: Response) => {
        console.log(err);
        return Observable.throw(err.json());
      });
  }

  login = ({ token, userId, firstName, lastName, email }) => {
    this.currentUser = new User(
      email,
      null,
      firstName,
      lastName,
      userId
    );
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify({ userId, firstName, lastName, email }));
    this.router.navigate(['/']);
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

  logout() {
    localStorage.clear();
    this.currentUser = null;
    this.router.navigateByUrl('/');
  }

  public showError = (msg: any) => {
    this.snackbar.open(msg, 'x', {duration: 2500});
  }

  public handleError = (err: any) => {
    const {error: { name }, message} = err;
    if (name === 'TokenExpiredError') {
      this.showError('Tu sesión ha expirado');
    } else if (name === 'JsonWebTokenError') {
      this.showError('Ha habido un problema con tu sesión');
    } else {
      this.showError(message || 'Ha ocurrido un error, intentalo nuevamente');
    }
    this.logout();
  }
}
