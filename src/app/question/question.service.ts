import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Question } from './question.model';
import { Answer } from '../answer/answer.model';
import { environment } from '../../environments/environment';
import urljoin from 'url-join';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class QuestionService {

  private questionsUrl: string;

  constructor(private http: Http) {
    this.questionsUrl = urljoin(environment.apiUrl, 'questions');
  }

  getQuestions(): Promise<void | Question[]> {
    return this.http.get(this.questionsUrl)
        .toPromise()
        .then(response => response.json() as Question[])
        .catch(this.handleError);
  }

  getQuestion(id): Promise<void | Question> {
     const url = urljoin(this.questionsUrl, id);
     return this.http.get(url)
         .toPromise()
         .then(response => response.json() as Question)
         .catch(this.handleError);
  }

  handleError(error: any) {
    const errMsg = error.message ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    console.log(errMsg);
  }

  getTokenInUrl(url) {
    const token = localStorage.getItem('token');
    return url + `?token=${token}`;
  }

  addQuestion(question: Question) {
    const body = JSON.stringify(question);
    const headers = new Headers({ 'Content-Type' : 'application/json' });

    const url = this.getTokenInUrl(this.questionsUrl);

    return this.http.post(url, body, { headers })
      .map((response: Response) => response.json())
      .catch((err: Response) => Observable.throw(err.json()));
  }

  addAnswer(answer: Answer) {
    const a = {
      description: answer.description,
      question: {
        _id: answer.question._id
      }
    };

    const body = JSON.stringify(a);
    const headers = new Headers({ 'Content-Type' : 'application/json' });
    const url = urljoin(this.questionsUrl, answer.question._id, 'answers');
    const token = localStorage.getItem('token');

    return this.http.post(this.getTokenInUrl(url), body, { headers })
      .map((response: Response) => response.json())
      .catch((err: Response) => Observable.throw(err.json()));
  }
}
