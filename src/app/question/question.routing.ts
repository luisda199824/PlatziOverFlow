import { QuestionFormComponent } from './question-form.component';
import { QuestionDetailComponent } from './question-detail.component';
import { QuestionListComponent } from './question-list.component';

export const QUESTION_ROUTES = [
  { path: '', component: QuestionListComponent, pathMatch: 'full'},
  { path: 'new', component: QuestionFormComponent},
  { path: ':id', component: QuestionDetailComponent},
];
